#!/usr/bin/env bash

set -e

######################## Declaring helpers ########################
# Log helper
function log() {
	echo "$@"
}

# Warn log helper
function warn() {
	log "WARNING> $* ($(caller))"
}

# Error log helper
function error() {
	log "ERROR> $* ($(caller))"
	exit 1
}

######################## Executing ########################
CMD=("$@")
if [[ -v ENTRYPOINT ]]; then
	CMD=("$ENTRYPOINT" "${CMD[@]}")
	unset ENTRYPOINT
fi

if [[ -v HOSTS ]]; then
	(
		IFS=','
		for host in $HOSTS; do echo "$host"; done
	) >>"/etc/hosts"
	unset HOSTS
fi

if [[ -n "${MP_USER:-""}" ]]; then
	M_USER="$MP_USER"
	# Set UID and GID default value if not set
	M_UID="${MP_UID:-1000}"
	M_GID="${MP_GID:-1000}"
	HOME_PATH="/home/$M_USER"

	WORKDIR=$(realpath "$PWD")

	readonly M_USER
	readonly M_UID
	readonly M_GID
	readonly HOME_PATH
	readonly WORKDIR

	# Unset the environment variables
	unset MP_USER
	unset MP_UID
	unset MP_GID

	if [[ -d "$HOME_PATH" ]]; then
		if [[ "$HOME_PATH" == "$WORKDIR" ]] || [[ "$HOME_PATH" = "$WORKDIR/*" ]]; then
			# HOME_PATH is a subdir of WORKDIR
			[[ $(stat -c '%u' "$HOME_PATH") == "$M_UID" ]] || error "UID $M_UID is not owner of $HOME_PATH" 1>&2
		else
			# Set user owner of it's home
			chown "$M_UID":"$M_GID" "$HOME_PATH"
		fi
	fi

	# Create group and user
	[[ -n $(getent group "$M_GID") ]] || addgroup --gid "$M_GID" "$M_USER" >/dev/null
	[[ -n $(getent passwd "$M_UID") ]] ||
		adduser --disabled-password --gecos "" --shell /bin/bash --home "$HOME_PATH" --uid "$M_UID" --gid "$M_GID" "$M_USER" >/dev/null

	# Add user to sudoers
	echo "$M_USER ALL=(ALL) NOPASSWD: ALL" >"/etc/sudoers.d/$M_USER"

	# Copy files to home only
	# if HOME_PATH is not a subdir of WORKDIR
	# and WORKDIR != HOME_PATH
	if [[ "$HOME_PATH/" != "$WORKDIR/*" ]] && [[ "$WORKDIR" != "$HOME_PATH" ]]; then
		# Copy files from current user home to HOME_PATH
		find ~/ -mindepth 1 -maxdepth 1 -type f -exec cp {} "$HOME_PATH" \;

		# Change HOME_PATH files owner
		find "$HOME_PATH" -mindepth 1 -maxdepth 1 -type f -uid "$(id --user root)" -exec chown "$M_UID":"$M_GID" {} \;
	fi

	PATH="${EXTRAPATH//~/$HOME_PATH}:/opt/elf/scripts/:$PATH"

	# Execute the command with created user
	gosu "$M_USER" "${CMD[@]}"
else
	exec "${CMD[@]}"
fi
