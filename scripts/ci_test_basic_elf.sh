#!/bin/bash

set -e

# shellcheck disable=SC2317
clean_up() {
    cd ..
    rm -rf elftesttmp
}

trap clean_up EXIT

############################################################
# Global variables
############################################################

SCRIPT_DIR=$(dirname "$(realpath "$0")")
ELF_DIR=$(dirname "$SCRIPT_DIR")

############################################################

if [ -d "./elftesttmp" ]; then
    rm -rf elftesttmp
fi

mkdir elftesttmp
cd elftesttmp

# Negative test
if "$ELF_DIR"/elfdocker -b; then
    exit 1
fi

if "$ELF_DIR"/elfrun; then
    exit 1
fi

"$ELF_DIR"/elfmanifest --verbose
"$ELF_DIR"/elfmanifest -c -n -p elftestproject

"$ELF_DIR"/elfdocker -c elftestproject
"$ELF_DIR"/elfdocker -s | grep -vq elftestproject
"$ELF_DIR"/elfdocker -b
"$ELF_DIR"/elfdocker -s | grep -q elftestproject
"$ELF_DIR"/elfdocker -c elftestproject
"$ELF_DIR"/elfdocker -s | grep -vq elftestproject

"$ELF_DIR"/elfrun | grep WARNING

"$ELF_DIR"/elfrun -- ls -la | grep ".manifest.xml"

"$ELF_DIR"/elfdocker -c elftestproject

# Test the fetcher_v2 
# Negative test with error in the fetch directive (no url)
rm ".manifest.xml"

{
    echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
    echo "<manifest version='1'>"
    echo "<default name=\"elftestproject\" version=\"0.0.0\" />"
    echo "<fetch type='git' name='poky'/>"
    echo "</manifest>"
} >>".manifest.xml"

if "$ELF_DIR"/elfrun -- ls -la ; then
    exit 1
fi

# Positive test : Cloning poky
rm ".manifest.xml"

{
    echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
    echo "<manifest version='1'>"
    echo "<default name=\"elftestproject\" version=\"0.0.0\" />"
    echo "<fetch type='git' name='poky' url='https://git.yoctoproject.org/poky'/>"
    echo "</manifest>"
} >>".manifest.xml"

"$ELF_DIR"/elfrun -- ls -la | grep "poky"

exit 0
