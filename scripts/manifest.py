#!/usr/bin/env python3

import os, sys

# This function print on stderr the caler of this program with minimal dependency
# It is useful to debug unwanted call to this function
def print_caller():
    elfppid = os.getppid()
    print(f"Exécuté par {elfppid}" , file=sys.stderr)
    file = open(f"/proc/{elfppid}/cmdline", "r")
    lines = file.readlines()
    file.close()
    for line in lines:
        print(f"{line.strip()} ", file=sys.stderr)


# Uncomment the line below to print the caller arguments
# print_caller()

from argparse import ArgumentParser, ArgumentTypeError
from pathlib import Path
from typing import Dict, Generator, Iterable, TextIO, Tuple, Union

import subprocess
from lxml import etree


def get_path(path: Path, working_dir: Path, absolute: bool) -> Path:
    abs_path = Path(path).absolute()

    if absolute:
        return abs_path
    else:
        return os.path.relpath(abs_path, working_dir.absolute())


def iter_xml(
    file: str, filters: Union[str, Iterable[str]] = "*"
) -> Generator[Tuple[str, Dict[str, str]], None, None]:
    root = etree.parse(file).getroot()
    if root.tag != "manifest":
        return

    if isinstance(filters, str):
        filters = [filters]

    try:
        for xpath_filter in filters:
            root.xpath(xpath_filter)
    except etree.XPathEvalError as exc:
        raise ValueError(f"Invalid XPath expression: '{xpath_filter}'") from exc

    for xpath_filter in filters:
        for child in root.findall(xpath_filter):
            yield child.tag, child.attrib


def existing_file_path(arg: str) -> str:
    try:
        file_path = Path(arg)
        assert file_path.exists()
        assert file_path.is_file()
        return arg
    except Exception as exc:
        raise ArgumentTypeError(
            f"'{arg}' shall match to an existing file path."
        ) from exc


def manifest_main(args):
    parser = ArgumentParser(description="List tag attributes from a manifest file")

    parser.add_argument(
        "--manifest",
        help="Manifest file path, default: .manifest.xml",
        default=".manifest.xml",
        type=existing_file_path,
    )

    parser.add_argument(
        "filters",
        nargs="*",
        type=str,
        help="XPath filters, " "see https://www.w3schools.com/xml/xpath_syntax.asp",
    )

    parser.add_argument("--tag", "-t", type=str, help="Select XML tag")

    parser.add_argument(
        "--basic-output",
        "-b",
        action="store_true",
        help="Basic output print, easy to use in bash scripts",
    )

    namespace = parser.parse_args(args)
    retval = ""
    try:
        for tag, attrib in iter_xml(namespace.manifest, namespace.filters):
            if namespace.tag:
                if namespace.tag in attrib:
                    if namespace.basic_output:
                        retval += f"{attrib[namespace.tag]}\n"
                    else:
                        retval += f"{tag}: '{attrib[namespace.tag]}'\n"
                else:
                    continue
            else:
                value = " ".join(f"{k}::{v}" for k, v in attrib.items())
                retval += f"{value}\n"

    except etree.XMLSyntaxError as exc:
        parser.error(exc)
    except ValueError as exc:
        parser.error(exc)
    return retval[:-1]


if __name__ == "__main__":
    args = sys.argv
    args.pop(0)
    retval = manifest_main(args)
    print(retval)
