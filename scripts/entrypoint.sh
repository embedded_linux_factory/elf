#!/usr/bin/env bash

set -e
NORMAL='\033[0m'
REDBOLD='\033[1;31m'
YELLOWBOLD='\033[1;33m'
export NORMAL REDBOLD YELLOWBOLD
############################################################
# Global variables
############################################################
ELFDIR="$(dirname "$(realpath "$0")")/.."
readonly ELFDIR

PLUGINS=""

ELF_MANIFEST=$(realpath "$ELF_MANIFEST")
export ELF_MANIFEST
############################################################
# Useful Tools
############################################################

# Simple log helper
function log() {
    if [ "$ELF_VERBOSE_MODE" == "1" ]; then
        printf "%s\n" "$*"
    fi
}
export -f log

# Warn log helper
function warn() {
    printf "%bWARNING> %s%b\n" "$YELLOWBOLD" "$*" "$NORMAL"
}
export -f warn

# Error log helper
function error() {
    printf "%bERROR> %s%b\n" "$REDBOLD" "$*" "$NORMAL"
    exit 1
}
export -f error

exec_args() {
    if [ -x "$1" ] || [ -x "$(which "$1")" ]; then
        log "Executing [$*]"
        exec "$@"
    else
        error "$1 not found or not executable"
    fi
}

############################################################
# Usage
############################################################
Usage() {
    printf "\nUsage:\n"
    printf "  %s [options]\n" "$(basename "$0")"
    printf "\nOptions:\n"
    printf "  -h                      Display this help message\n"
    printf "  -c <config>             Override ELF_ENV_CONFIG with the specified configuration\n"
    printf "\nExample:\n"
    printf "  %s -c myconfig ls -la\n" "$(basename "$0")"
    printf "\n"
}

############################################################
# Args check
############################################################
while getopts "hc:" option; do
    case $option in
    c)
        ELF_ENV_CONFIG=$OPTARG
        readonly ELF_ENV_CONFIG
        ;;
    h)
        Usage
        exit 0
        ;;
    *)
        Usage
        exit
        ;;
    esac
done

############################################################
# Entrypoint
############################################################

####
# Display the welcome page
####
if [ "$ELF_VERBOSE_MODE" == "1" ]; then
    "$ELFDIR"/scripts/elf-welcome.sh
fi

####
# Check if the config is set and valid
####
if [ -n "$ELF_ENV_CONFIG" ]; then
    if [ -z "$("$ELFDIR"/scripts/manifest.py --manifest "$ELF_MANIFEST" config[@name=\'"$ELF_ENV_CONFIG"\'])" ]; then
        error "Unknown configuration: [$ELF_ENV_CONFIG]"
    fi
else
    ELF_ENV_CONFIG="$($ELFDIR/scripts/manifest.py --manifest "$ELF_MANIFEST" config --tag name --basic-output | head -n1)"
    if [ -z "$ELF_ENV_CONFIG" ]
    then
        ELF_ENV_CONFIG="noconfig"
        log No default configuration found
    fi
fi
log "Using configuration: [$ELF_ENV_CONFIG]"
export ELF_ENV_CONFIG

####
# Fetch dependencies
####
"$ELFDIR"/scripts/fetch.sh
"$ELFDIR"/scripts/fetcher_v2.sh -m "$ELF_MANIFEST"

#### Run plugins specific init.sh and format-env.sh scripts
PLUGINS="$("$ELFDIR"/scripts/manifest.py --manifest "$ELF_MANIFEST" plugin --tag name --basic-output)"
for plugin in $PLUGINS; do
    if [ -x "$ELFDIR/plugins/$plugin/init.sh" ]; then
        "$ELFDIR"/plugins/"$plugin"/init.sh "$ELF_ENV_CONFIG"  "$ELF_INTERACTIVE_MODE"
    else
        log "No entrypoint callback for plugin [$plugin]"
    fi
    if [[ -x "$ELFDIR/plugins/$plugin/format-env.sh" ]]; then
        IFS=" " read -r -a PLUGINENV_ARR <<< "$("$ELFDIR/plugins/$plugin/format-env.sh" "$ELF_ENV_CONFIG")"
        # shellcheck source=/dev/null
        source "${PLUGINENV_ARR[@]}"
    else
        log "No environment to source for plugin [$plugin]"
    fi
done

####
# Execution
####
# No docker mode
if [ "$ELF_NO_DOCKER_MODE" == "1" ]; then
    if [ $# == 0 ]; then
        bash
    else
        exec_args "$@"
    fi
# Docker mode
elif [ -n "$1" ] && [ "$1" == "/bin/bash" ]; then
    if [ "$ELF_INTERACTIVE_MODE" == "1" ]; then
        log "#####     Run interactive mode     #####"
        bash
    else
        warn "Nothing to do. You are running non interactive mode using default bash command"
    fi
else
    exec_args "$@"
fi

exit 0
