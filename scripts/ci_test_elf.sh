#!/bin/bash

# Exit immediately if a command exits with a non-zero status
set -e

for plugin in plugins/*; do
    ci_test="$plugin/ci_test.sh"
    if [[ -x $ci_test ]]; then
        $ci_test
    else
        echo "No test for plugin $(basename "$plugin")"
    fi
done

exit 0
