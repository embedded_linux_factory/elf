#!/usr/bin/env bash

set -e

######################## Declaring global variable ########################
readonly ELFDIR="$(dirname $(realpath "$0"))/.."

if [ ! -z "$ELF_MANIFEST" ]
then
    readonly MANIFEST_PATH=$ELF_MANIFEST
else
    readonly MANIFEST_PATH=".manifest.xml"
fi

######################## Executing ########################
log '#####     Fetch sources     #####'

default_branch="$($ELFDIR/scripts/manifest.py --manifest=${MANIFEST_PATH} default --tag revision --basic-output)"
if [[ -z "${default_branch}" ]]
then
    plugins="$($ELFDIR/scripts/manifest.py --manifest=${MANIFEST_PATH} plugin --tag name --basic-output)"
    for plugin in ${plugins}; do
        if [[ -x "$ELFDIR/plugins/${plugin}/fetch_get_default_branch.sh" ]]; then
            default_branch=$("$ELFDIR/plugins/${plugin}/fetch_get_default_branch.sh")
        fi
    done
    [[ -n "${default_branch}" ]] || default_branch="master"
fi
log "Default branch: $default_branch"

# For the CI, which doesn't get its project sources with git
if [[ -d ".git" ]]; then
    # Keep branch and revision of the project
    project_name="$($ELFDIR/scripts/manifest.py --manifest=${MANIFEST_PATH} default --tag name --basic-output)"
    final_branch="$(git branch --show-current)"
    final_revision="$(git log --format="%H" -n 1)"
    echo "$project_name $final_branch::$final_revision" > revisions.txt
fi

for pn in $($ELFDIR/scripts/manifest.py --manifest=${MANIFEST_PATH} config[@name=\'$ELF_ENV_CONFIG\']/project project --tag name --basic-output); do
    path="$($ELFDIR/scripts/manifest.py --manifest=${MANIFEST_PATH} config[@name=\'$ELF_ENV_CONFIG\']/project[@name=\'$pn\'] project[@name=\'$pn\'] --tag path --basic-output)"
    [[ -n "${path}" ]] || path="sources/${pn}"
    revision="$($ELFDIR/scripts/manifest.py --manifest=${MANIFEST_PATH} config[@name=\'$ELF_ENV_CONFIG\']/project[@name=\'$pn\'] project[@name=\'$pn\'] --tag revision --basic-output)"
    branch="$($ELFDIR/scripts/manifest.py --manifest=${MANIFEST_PATH} config[@name=\'$ELF_ENV_CONFIG\']/project[@name=\'$pn\'] project[@name=\'$pn\'] --tag branch --basic-output)"
    [[ -n "${branch}" ]] || branch="${default_branch}"
    remote="$($ELFDIR/scripts/manifest.py --manifest=${MANIFEST_PATH} config[@name=\'$ELF_ENV_CONFIG\']/project[@name=\'$pn\'] project[@name=\'$pn\'] --tag remote --basic-output)"
    remote_url="$($ELFDIR/scripts/manifest.py --manifest=${MANIFEST_PATH} config[@name=\'$ELF_ENV_CONFIG\']/remote[@name=\'$remote\'] remote[@name=\'$remote\'] --tag fetch --basic-output)"
    nogitext="$($ELFDIR/scripts/manifest.py --manifest=${MANIFEST_PATH} config[@name=\'$ELF_ENV_CONFIG\']/project[@name=\'$pn\'] project[@name=\'$pn\'] --tag nogitext --basic-output)"
    if [[ x"${nogitext}" == "xyes" ]]; then
        ext=""
    else
        ext=".git"
    fi
    need_clone="no"

    if [[ -d "${path}" ]]; then
        cd "${path}"
        current_status="$(git status -s)"
        # Checking local changes
        if [[ -n "${current_status}" ]]; then
            log "Changes detected on sources [${path}] :"
            log " ${current_status}"
            log ""
            log "Skip until changes are applied or removed"
            cd - > /dev/null
            continue
        fi
        # Checking remote
        current_remote="$(git remote -v | awk '/origin/ && /fetch/{print $2}')"
        if [[ "${remote_url}/${pn}${ext}" != "${current_remote}" ]]; then
            log "We have to replace the remote:"
            log "  expected: ${remote_url}/${pn}${ext}"
            log "  current: ${current_remote}"
            need_clone="yes"
        fi
        cd - > /dev/null
    else
        need_clone="yes"
    fi
    # if the layer doesn't exists, we clone it
    if [[ "xyes" = "x${need_clone}" ]]; then
        rm -rf "${path}"
        log "Cloning [${remote_url}/${pn}${ext}] in [${path}] with branch [${branch}]"
        git clone "${remote_url}/${pn}${ext}" "${path}" -b "${branch}"
        # checkout the revision if specified
        if [[ -n "${revision}" ]]; then
            cd "${path}"
            git reset --hard "${revision}"
            cd - > /dev/null
        fi
    fi
    # Once the layer exists, we check that we use the correct revision
    if [[ -n "${revision}" ]]; then
        cd "${path}"
        current_revision="$(git log --format="%H" -n 1)"
        if [[ "${revision}" != "${current_revision}" ]]; then
            git fetch
            log "${pn} current revision [${current_revision}] differ from the expected one [${revision}]"
            git reset --hard "${revision}"
            git status
        fi
        cd - > /dev/null
    else
        cd "${path}"
        current_branch="$(git branch --show-current)"
        if [[ "${branch}" != "${current_branch}" ]]; then
            log "${pn} current branch [${current_branch}] differ from the expected one [${branch}]: switching ..."
            git checkout "${branch}"
        fi
        cd - > /dev/null
    fi

    # Keep branch and revision of the layer
    cd "${path}"
    final_branch="$(git branch --show-current)"
    final_revision="$(git log --format="%H" -n 1)"
    cd - > /dev/null
    echo "$pn $final_branch::$final_revision" >> revisions.txt
done

plugins="$($ELFDIR/scripts/manifest.py --manifest=${MANIFEST_PATH} plugin --tag name --basic-output)"
for plugin in ${plugins}; do
    if [[ -x "$ELFDIR/plugins/${plugin}/fetch.sh" ]]; then
        $ELFDIR/plugins/${plugin}/fetch.sh
    else
        log "No fetch callback for plugin [${plugin}]"
    fi
done
