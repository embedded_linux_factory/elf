#!/usr/bin/env bash

set -e
NORMAL='\033[0m'
REDBOLD='\033[1;31m'

############################################################
# Global variables
############################################################
ELF_DIR="$(dirname "$(realpath "$0")")/.."
readonly ELF_DIR

PROJECT_DIR="$(pwd)"
readonly PROJECT_DIR

MANIFEST_PATH="$PROJECT_DIR/.manifest.xml"

FETCHER_DIR="$PROJECT_DIR/fetcher_sources"

############################################################
# Usage
############################################################
Usage() {
    printf "\nUsage:\n"
    printf "  %s [options]\n" "$(basename "$0")"
    printf "\nOptions:\n"
    printf "  -m <path/to/manifest>   Path to the manifest file (default: %s)\n" "$MANIFEST_PATH"
    printf "  -h                      Display this help message\n"
    printf "\nExample:\n"
    printf "  %s -m /path/to/manifest.xml\n" "$(basename "$0")"
    printf "\n"
}

############################################################
# Args check
############################################################
while getopts "hm:" option; do
    case $option in
    m)
        MANIFEST_PATH=$(realpath "$OPTARG")
        ;;
    h)
        Usage
        exit 0
        ;;
    *)
        Usage
        exit
        ;;
    esac
done

# FIX the variable
readonly MANIFEST_PATH

############################################################
# Useful Tools
############################################################

#######################################
# Get all the set of attributes of a specific element from
# the manifest
# If a tag is also specify, it return the attribute value(s)
# of that tag
# Globals:
#   ELF_DIR
#   MANIFEST_PATH
# Arguments:
#   $1: XML elements where to search
#   $2: attribute tag to get the value from
# Outputs:
#   Writes the values to stdout
#######################################
get_manifest_attributes() {
    local elements="$1"
    local tag="$2"
    local tmp_val

    # shellcheck disable=SC2086
    tmp_val="$("$ELF_DIR"/scripts/manifest.py --manifest "$MANIFEST_PATH" $elements --tag "$tag" --basic-output)"
    echo "$tmp_val"
}

#######################################
# Download a file from an url
# Globals:
#   None
# Arguments:
#   $1: URL to download the file from
# Outputs:
#   Writes the file in fetcher_sources/downloads
#######################################
download_file() {
    printf "File downloader\n"

    local url=$1
    local tmp_path="$FETCHER_DIR/downloads"

    # #### DEBUG
    # printf "URL: %s\n" "$url"
    # ####

    if [ -z "$url" ]; then
        printf "%download_file() need an url set as first arg%b\n" "$REDBOLD" "$NORMAL"
        exit 1
    fi

    if [ ! -d "$tmp_path" ]; then
        mkdir -p "$tmp_path"
    fi

    #### Downloading from the url
    cd "$tmp_path"
    curl --url "$url" --output "$(basename "$url")"
    printf "File downloaded from [%s] in [%s]\n" "$url" "$tmp_path"
    cd - >/dev/null
}

#######################################
# GIT handler
#######################################
handler_git() {
    if [ "$ELF_VERBOSE_MODE" == "1" ]; then
        printf "Git handler\n"
    fi
    local -n local_attr_dict=$1

    local name
    local path
    local url
    local sha1_rev
    local branch

    if [ ! "${#local_attr_dict[@]}" -ne 0 ]; then
        printf "%bhandler_git() need an element attributes set as first arg%b\n" "$REDBOLD" "$NORMAL"
        exit 1
    fi

    #### DEBUG
    if [ "$ELF_VERBOSE_MODE" == "1" ]; then
        for key in "${!local_attr_dict[@]}"; do
            echo "$key: ${local_attr_dict[$key]}"
        done
    fi
    ####

    # Get the necessary values, only the name and url are
    # mandatory
    name=${local_attr_dict["name"]}
    url=${local_attr_dict["url"]}

    if [ -z "$name" ] || [ -z "$url" ]; then
        printf "%bName and url are mandatory, check the \`fetch\` elements of your manifest%b\n" "$REDBOLD" "$NORMAL"
        exit 1
    fi

    # Default path to fetcher_sources/$name
    path=${local_attr_dict["path"]}
    if [ -z "$path" ]; then
        path="$FETCHER_DIR/$name"
    fi

    sha1_rev=${local_attr_dict["sha1-rev"]}
    branch=${local_attr_dict["branch"]}

    #### Check if the repo need to be cloned or updated
    local current_status
    local current_remote
    local need_clone="no"

    if [[ -d "${path}" ]]; then
        cd "${path}"
        current_status="$(git status -s)"

        # If local changes, do nothing but print warning
        if [[ -n "${current_status}" ]]; then
            printf "Changes detected on sources [%s] :\n" "$path"
            printf "%s\n" "$current_status"
            printf "Skip until changes are applied or removed\n"
        else
            # If no local change but remote change, we reclone
            current_remote="$(git remote -v | awk '/origin/ && /fetch/{print $2}')"
            # TODO : Check only the relevant part of the url
            # so HTTPS and SSH url are interchangeable
            if [ "$current_remote" != "${url}" ]; then
                printf "Current remote [%s] differ from the expected one [%s]\n" "$current_remote" "$url"
                need_clone="yes"
            fi
        fi

        cd - >/dev/null
    else
        need_clone="yes"
    fi

    #### Cloning the repo if needed
    if [[ "${need_clone}" == "yes" ]]; then

        printf "Cloning [%s] in [%s]" "${url}" "${path}"

        local branch_option
        if [[ -n "${branch}" ]]; then
            branch_option="-b ${branch}"
            printf " with branch [%s]\n" "${branch}"
        else
            branch_option=""
            printf "\n"
        fi

        rm -rf "${path}"
        # shellcheck disable=SC2086
        git clone "${url}" "${path}" ${branch_option}

        # If there is a revision, we checkout the revision
        # (the branch is ignored)
        if [[ -n "${sha1_rev}" ]]; then
            printf "Revision specified, checking out [%s]\n" "${sha1_rev}"
            cd "${path}"
            git reset --hard "${sha1_rev}"
            cd - >/dev/null
        fi
    fi

    #### Final Check : the revision and branch
    # We need to ensure that the revision is the correct
    # one
    local current_rev
    local current_branch
    local relevant_branch
    local relevant_rev

    cd "${path}"

    if [ "$ELF_VERBOSE_MODE" == "1" ]; then
        git fetch
    else
        git fetch 2> /dev/null
    fi

    if [[ -n "${sha1_rev}" ]]; then

        # If a revision is specified, it need to be the
        # current revision of the repo

        current_rev="$(git log --format="%H" -n 1)"
        if [[ "${sha1_rev}" != "${current_rev}" ]]; then
            # The aim here is to not create a detached head
            # so we need to find the branch that correspond
            # to the revision
            relevant_branch="$(git name-rev --name-only "${sha1_rev}" | awk '{print $1}' | sed 's/~[0-9]*//' | awk '{sub(/^remotes\/origin\//, ""); print}')"
            printf "Current revision [%s] differ from the expected one [%s] from the branch [%s], switching ...\n" "${current_rev}" "${sha1_rev}" "${relevant_branch}"
            git switch "${relevant_branch}"
            git reset --hard "${sha1_rev}"
        fi
    elif [[ -z "${branch}" ]]; then

        # If no rev and no branch specified, the revision
        # should be the head of the repo default branch
        # (main, master etcc)

        relevant_branch="$(git remote show origin | awk '/HEAD branch/{print $3}')"
        current_rev="$(git log --format="%H" -n 1)"

        if [ "$current_rev" != "$(git rev-parse origin/"${relevant_branch}")" ]; then
            printf "No revision and no branch specified, the current revision should be the head of the default branch\n"
            printf "Current revision [%s] differ from the head of the default branch [%s], resetting ...\n" "$(git rev-parse HEAD)" "$(git rev-parse origin/"${relevant_branch}")"
            git reset --hard "origin/${relevant_branch}"
            git checkout "${relevant_branch}"
        fi

    else

        # If no revision specified but a branch yes,
        # the current revision should be the head of the
        # specified branch

        current_rev="$(git log --format="%H" -n 1)"
        relevant_rev="$(git rev-parse origin/"${branch}")"

        if [ "$current_rev" != "$relevant_rev" ]; then

            relevant_branch="$(git name-rev --name-only "${relevant_rev}" | awk '{print $1}' | sed 's/~[0-9]*//' | awk '{sub(/^remotes\/origin\//, ""); print}')"
            printf "No revision specified but a branch yes, the current revision should be the head of the specified branch\n"
            printf "Current revision [%s] differ from the head of the branch specified [%s], resetting ...\n" "$(git rev-parse HEAD)" "$(git rev-parse origin/"${branch}")"
            git switch "${relevant_branch}"
            git reset --hard "origin/${branch}"
        fi
    fi

    # We record all the final information from the repo
    current_rev="$(git log --format="%H" -n 1)"
    current_branch="$(git branch --show-current)"

    cd - >/dev/null

    printf "%s %s::%s\n" "$name" "$current_branch" "$current_rev" >>"$PROJECT_DIR/revision.txt"

}

#######################################
# Debian package handler
#######################################
deb_handler() {
    printf "Debian package handler\n"

    local -n local_attr_dict=$1

    local name

    #### DEBUG
    # for key in "${!local_attr_dict[@]}"; do
    #     echo "$key: ${local_attr_dict[$key]}"
    # done
    ####

    name=$(basename "${local_attr_dict["url"]}")
    if [ -z "$name" ]; then
        printf "%bURL is mandatory, check the \`fetch\` elements of your manifest%b\n" "$REDBOLD" "$NORMAL"
        exit 1
    fi


    sudo dpkg -i "$FETCHER_DIR"/downloads/"$name"
    sudo ldconfig
}

#######################################
# Tarball handler
#######################################
tar_handler() {
    printf "Tar package handler\n"

    local -n local_attr_dict=$1

    local name
    local path

    #### DEBUG
    # for key in "${!local_attr_dict[@]}"; do
    #     echo "$key: ${local_attr_dict[$key]}"
    # done
    ####

    name=$(basename "${local_attr_dict["url"]}")
    if [ -z "$name" ]; then
        printf "%bURL is mandatory, check the \`fetch\` elements of your manifest%b\n" "$REDBOLD" "$NORMAL"
        exit 1
    fi

    path=${local_attr_dict["path"]}
    if [ -z "$path" ]; then
        path="$FETCHER_DIR/$name"
    fi

    if [ ! -d "$path" ]; then
        mkdir -p "$path"
    fi

    tar -xvzf "$FETCHER_DIR"/downloads/"$name" -C "$path"
}

#######################################
# Local handler
#######################################
local_handler() {
    printf "Local handler\n"

    local -n local_attr_dict=$1

    local output_path
    local local_path

    #### DEBUG
    # for key in "${!local_attr_dict[@]}"; do
    #     echo "$key: ${local_attr_dict[$key]}"
    # done
    ####

    local_path=${local_attr_dict["local_path"]}
    if [ -z "$local_path" ]; then
        printf "%bLocal path is mandatory, check the \`fetch\` elements of your manifest%b\n" "$REDBOLD" "$NORMAL"
        exit 1
    fi

    output_path=${local_attr_dict["path"]}
    if [ -z "$output_path" ]; then
        output_path="$FETCHER_DIR"
    fi

    if [ ! -d "$output_path" ]; then
        mkdir -p "$output_path"
    fi

    cp -v -r "$local_path" "$output_path"
}

############################################################

while IFS= read -r line; do

    # If no "fetch" directive in manifest, we do nothing
    if [ -z "$line" ]; then
        break
    fi

    # Create a dictionnary for the attributes
    declare -A attributes_dict
    for attribute in $line; do
        # shellcheck disable=SC2206
        attr_array=(${attribute//\:\:/ })
        attributes_dict["${attr_array[0]}"]="${attr_array[1]}"
    done

    # Redirect the current fetch directive to the right
    # handler
    case ${attributes_dict["type"]} in
    "git")
        handler_git attributes_dict
        ;;
    "deb")
        download_file "${attributes_dict["url"]}"
        deb_handler attributes_dict
        ;;
    "tar")
        download_file "${attributes_dict["url"]}"
        tar_handler attributes_dict
        ;;
    "local")
        local_handler attributes_dict
        ;;
    *)
        printf "%bUnknown type of fetch directive : [%s]%b\n" "$REDBOLD" "${attributes_dict["type"]}" "$NORMAL"
        ;;
    esac


    if [ "$ELF_VERBOSE_MODE" == "1" ]; then
        printf "\n\n"
    fi
    # Reset the dictionnary
    attributes_dict=()

done < <(get_manifest_attributes "fetch config[@name='$ELF_ENV_CONFIG']/fetch")
