#!/usr/bin/env bash

set -e

if [ "$EUID" -ne 0 ]; then
	echo "Please run as root"
	exit 1
fi

get_distribution() {
	lsb_dist=""
	# Every system that we officially support has /etc/os-release
	if [ -r /etc/os-release ]; then
		lsb_dist="$(. /etc/os-release && echo "$ID")"
	fi
	# Returning an empty string here should be alright since the
	# case statements don't act unless you provide an actual value
	echo "$lsb_dist"
}

lsb_dist="$(get_distribution | tr '[:upper:]' '[:lower:]')"

echo "Installing prerequisites"
case "$lsb_dist" in
ubuntu | debian)
	apt install -y git python3 python3-lxml
	;;
fedora)
	dnf install git python3 python3-lxml
	;;
*)
	echo "Distribution $lsb_dist is not supported"
	echo "Please set a valid one:"
	echo "    - ubuntu"
	echo "    - debian"
	echo "    - fedora"
	echo "If you need support on another distribution,"
	echo "please contact a maintainer"
	;;
esac

echo "Installing docker"
wget http://get.docker.com -O /tmp/get_docker.sh -q
chmod +x /tmp/get_docker.sh
/tmp/get_docker.sh
rm -rf /tmp/get_docker.sh

echo "To use elf add your account to docker group and then logout"
echo "/sbin/usermod -aG docker \$USER"
echo ""
echo "Optionnaly, you can install elf in /opt:"
echo "mkdir -p /opt/elf/ ; cp -r \$ELFDIR/* /opt/elf/ ; ln -sf /opt/elf/elf.py /usr/bin/elf.py"

exit 0
