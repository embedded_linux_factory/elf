#!/usr/bin/env bash

set -e

######################## Declaring global variable ########################
ELFDIR="$(dirname "$(realpath "$0")")/.."
readonly ELFDIR

####################### Testing ########################
plugins=$("$ELFDIR/scripts/manifest.py" --manifest="$ELF_MANIFEST" plugin --tag name --basic-output)
for plugin in $plugins; do
    if [[ -x "$ELFDIR/plugins/$plugin/test.sh" ]]; then
        "$ELFDIR/plugins/$plugin/test.sh" "$ELF_ENV_CONFIG"
    fi
done

COMMAND=$("$ELFDIR/scripts/manifest.py" --manifest="$ELF_MANIFEST" test --tag command --basic-output)
if [[ -n "$COMMAND" ]]; then
    $COMMAND
fi

SCRIPT=$("$ELFDIR/scripts/manifest.py" --manifest="$ELF_MANIFEST" test --tag script --basic-output)
if [[ -n "$SCRIPT" ]]; then
    DIR_NAME=$(dirname "$ELF_MANIFEST")
    COMMAND="$DIR_NAME/$SCRIPT"
    $COMMAND
fi

exit 0
