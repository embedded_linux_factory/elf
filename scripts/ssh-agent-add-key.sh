#!/usr/bin/env bash

set -e

eval "$(ssh-agent -s)"

[[ -n "$1" ]] || ssh-add "$1"

exit 0
