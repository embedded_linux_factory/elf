#!/usr/bin/env bash

set -e

######################## Declaring global variable ########################
ELFDIR="$(dirname "$(realpath "$0")")/../"
readonly ELFDIR

PARSE_MANIFEST="$ELFDIR/scripts/manifest.py"

######################## Declaring helpers ########################
# Log helper
function log() {
    echo "$@"
}
export -f log

# Warn log helper
function warn() {
    log "WARNING> $* ($(caller))"
}
export -f warn

# Error log helper
function error() {
    log "ERROR> $* ($(caller))"
    exit 1
}
export -f error

MANIFEST_PATH="$2"
if [[ -z "$MANIFEST_PATH" ]]; then
    MANIFEST_PATH=".manifest.xml"
fi

ELF_ENV_CONFIG="$3"
if [[ -z "$ELF_ENV_CONFIG" ]]; then
    ELF_ENV_CONFIG="$($PARSE_MANIFEST --manifest "$MANIFEST_PATH" config --tag name --basic-output | head -n1)"
    [[ -n "$ELF_ENV_CONFIG" ]] || ELF_ENV_CONFIG="noconfig"
fi

######################## Executing ########################
[[ -f "$MANIFEST_PATH" ]] || error "Manifest $MANIFEST_PATH not found please create one with 'elfmanifest'"

PROJECT_NAME="$($PARSE_MANIFEST --manifest="$MANIFEST_PATH" default --tag name --basic-output)"
[[ -n "$PROJECT_NAME" ]] || PROJECT_NAME="noname"

if [[ "$PROJECT_NAME" == "noname" ]]; then
    warn "Project name not set in manifest"
fi

if [[ "$1" == "build" ]]; then
    # Build template docker image
    plugins="$($PARSE_MANIFEST --manifest="$MANIFEST_PATH" plugin --tag name --basic-output)"

    ELFDOCKERBASETEMPLATE="$($PARSE_MANIFEST --manifest="$MANIFEST_PATH" default --tag template --basic-output)"
    if [[ -z "$ELFDOCKERBASETEMPLATE" ]]; then
        # if a plugin need a specific base template we change it
        for plugin in $plugins; do
            if [[ -x "$ELFDIR/plugins/$plugin/get_docker_base_template" ]]; then
                [[ -z "$ELFDOCKERBASETEMPLATE" ]] || error "Base template already set by another plugin to [$ELFDOCKERBASETEMPLATE]"
                ELFDOCKERBASETEMPLATE=$("$ELFDIR/plugins/$plugin/get_docker_base_template")
            fi
        done
        #if no template deduced, we set the default one
        [[ -n "$ELFDOCKERBASETEMPLATE" ]] || ELFDOCKERBASETEMPLATE="elf-ubuntu-2404"
    fi

    if [[ -z $(docker image ls "$ELFDOCKERBASETEMPLATE" -q) ]]; then
        log "Building $ELFDOCKERBASETEMPLATE"
        [[ -f "$ELFDIR/templates/Dockerfile-$ELFDOCKERBASETEMPLATE" ]] || error "unknown template [$1]"
        docker build -t "$ELFDOCKERBASETEMPLATE" "$ELFDIR/templates/" -f "$ELFDIR/templates/Dockerfile-$ELFDOCKERBASETEMPLATE"
    fi

    # Generate project Dockerfile
    MANIFEST_DIR=$(dirname "$(realpath "$MANIFEST_PATH")")
    PROJECT_TMP_DIR="/home/$(whoami)/.elf$MANIFEST_DIR/checksum"
    readonly PROJECT_TMP_DIR

    PROJECT_DOCKERFILE="$PROJECT_TMP_DIR/Dockerfile"

    if [[ -f "$PROJECT_DOCKERFILE" ]]; then
        PREVIOUS_PROJECT_DOCKERFILE_MD5=$(md5sum "$PROJECT_DOCKERFILE" | awk '{print $1}')
        readonly PREVIOUS_PROJECT_DOCKERFILE_MD5
    fi

    rm -rf "$PROJECT_TMP_DIR"
    mkdir -p "$PROJECT_TMP_DIR"

    # Fill project Dockerfile if change
    log "FROM $ELFDOCKERBASETEMPLATE:latest" >"$PROJECT_DOCKERFILE"
    for plugin in $plugins; do
        if [[ -x "$ELFDIR/plugins/$plugin/generate_dockerfile" ]]; then
            "$ELFDIR/plugins/$plugin/generate_dockerfile" "$PROJECT_DOCKERFILE"
        else
            log "No callback to generate Dockerfile for plugin [$plugin]"
        fi
    done

    EXTRA_DOCKER_PACKAGES="$($PARSE_MANIFEST --manifest="$MANIFEST_PATH" config[@name=\'"$ELF_ENV_CONFIG"\']/docker docker --tag package --basic-output | xargs)"
    if [[ -n "$EXTRA_DOCKER_PACKAGES" ]]; then
        log " \
        RUN export DEBIAN_FRONTEND=noninteractive \
        && apt-get -y update \
        && apt-get -y upgrade \
        && apt-get -y install $EXTRA_DOCKER_PACKAGES \
        && apt-get clean && rm -rf /var/lib/apt/lists \
        " >>"$PROJECT_DOCKERFILE"
    fi

    [[ -f "$PROJECT_DOCKERFILE" ]] || error "Error: unknown project Dockerfile [$1]"
    PROJECT_DOCKERFILE_MD5=$(md5sum "$PROJECT_DOCKERFILE" | awk '{print $1}')
    readonly PROJECT_DOCKERFILE_MD5

    IMAGE_NAME="elf-$PROJECT_NAME-$PROJECT_DOCKERFILE_MD5"
    if [[ -z $(docker image ls "$IMAGE_NAME" -q) ]]; then

        if [[ -n "$PREVIOUS_PROJECT_DOCKERFILE_MD5" ]] && [[ "$PREVIOUS_PROJECT_DOCKERFILE_MD5" != "$PROJECT_DOCKERFILE_MD5" ]]; then
            PREVIOUS_IMAGE_NAME="elf-$PROJECT_NAME-$PREVIOUS_PROJECT_DOCKERFILE_MD5"

            read -rp "New image. Do you want to delete previous image $PREVIOUS_IMAGE_NAME ? (y/n) " response
            if [[ "$response" == "y" ]]; then
                docker rmi "$PREVIOUS_IMAGE_NAME"
                log "Deleting $PREVIOUS_IMAGE_NAME."
            else
                log "Keeping $PREVIOUS_IMAGE_NAME."
            fi
        fi

        log "Building $IMAGE_NAME"
        docker build -t "$IMAGE_NAME" "$PROJECT_TMP_DIR" -f "$PROJECT_DOCKERFILE"
    else
        log "Image already exists."
    fi

elif [[ "$1" == "clean" ]]; then
    ids=$(docker image ls "elf-$PROJECT_NAME-*" | grep elf- | awk '{print $3}' | xargs)
    [[ -z "$ids" ]] || docker image rm -f "$ids" >/dev/null
else
    error "Unknown arguments [$1]"
fi

exit 0
