import re

def check_ip(ip : str):
    if re.match(r"[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}", ip):
        t = ip.split('.')
        if len(t) == 4:
            ok = 0
            for e in t:
                if int(e) >= 0 and int(e) <256:
                    ok += 1
            if ok == 4:
                return True
    return False    

def sanitize_hosts_list(h : str):
    ret = []
    hosts=h.split(',')
    for host in hosts:
        value=host.split()
        if len(value) >= 2:
            if check_ip(value[0]):
                ret += [ value[0] + " " + value[1] ]
    return ','.join(ret)