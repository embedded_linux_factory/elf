#!/bin/bash

maintainers="Fabien Lahoudere <f.lahoudere@technologyandstrategy.com>" #To add more, use this patterns: Name1, Name2, Name3 ....
debian_builder_path=$(dirname "$(realpath "$0")")
project_root=$(dirname "$debian_builder_path")
# Exit immediately if a command exits with a non-zero status
set -e

#Verify the inputs number
if [ $# -ne 1 ]; then
  echo "Usage : $0 Package_version"
  echo "With : Package_version is the version of elf package that has the format x.x.x"
  exit 1
fi
#Verify the version format
if ! [[ "$1" =~ ^(0|[1-9][0-9]{0,1})\.([0-9]|[1-9][0-9]{0,1})\.([0-9]|[1-9][0-9]{0,1})$ ]]; then
  echo "Usage : $0 Package_version"
  echo "With : Package_version is the version of elf package that has the format x.x.x"
  echo "       x is a digit from 0 to 99"
  exit 1
fi

package_folder_name="elf_$1-1_amd64"

#Create the folder of package generation
if ! [ -d "$debian_builder_path/tmp" ]; then
  mkdir "$debian_builder_path/tmp"
fi
package_folder_name_absolute="$debian_builder_path/tmp/$package_folder_name"
mkdir "$package_folder_name_absolute"
mkdir "$package_folder_name_absolute/DEBIAN"

# Create licence file
cp "$debian_builder_path/components/copyright" "$package_folder_name_absolute/DEBIAN/copyright"

# Create readme source file
cp "$debian_builder_path/../README.md" "$package_folder_name_absolute/DEBIAN/README.Debian"

#Create the control file
touch "$package_folder_name_absolute/DEBIAN/control"
{
  echo "Package: elf"
  echo "Version: $1"
  echo "Maintainer: ${maintainers}"
  echo "Architecture: all"
  echo "Essential: no"
  echo "Priority: optional"
  echo "Depends: python3 (>=3.8), python3-pip (>=20.0), docker-ce (>=20.10), docker-ce-cli (>=20.10), containerd.io (>=1.6), docker-compose-plugin (>=2.6)"
  echo "Description: Elf or Embedded Linux Factory is a project to compile custom yocto images for customer or internal projects made by T&S company."
} >>"$package_folder_name_absolute/DEBIAN/control"

#Create the changelog file
echo Todo "$package_folder_name_absolute/DEBIAN/changelog"

#Create the post installation script
cp "$debian_builder_path/components/postinst" "$package_folder_name_absolute/DEBIAN/postinst"
chmod +x "$package_folder_name_absolute/DEBIAN/postinst"

#Create the pre-installation script
cp "$debian_builder_path/components/preinst" "$package_folder_name_absolute/DEBIAN/preinst"
chmod +x "$package_folder_name_absolute/DEBIAN/postinst"

#Create the pre-uninstallation script
cp "$debian_builder_path/components/prerm" "$package_folder_name_absolute/DEBIAN/prerm"
chmod +x "$package_folder_name_absolute/DEBIAN/prerm"

#Create the post-uninstallation script
cp "$debian_builder_path/components/postrm" "$package_folder_name_absolute/DEBIAN/postrm"
chmod +x "$package_folder_name_absolute/DEBIAN/postrm"

# build the elf files architecture

## Elf file
mkdir "$package_folder_name_absolute/usr"
mkdir "$package_folder_name_absolute/usr/share"
mkdir "$package_folder_name_absolute/usr/share/elf"
cp "$project_root/elfdocker" "$package_folder_name_absolute/usr/share/elf/elfdocker"
cp "$project_root/elfmanifest" "$package_folder_name_absolute/usr/share/elf/elfmanifest"
cp "$project_root/elfrun" "$package_folder_name_absolute/usr/share/elf/elfrun"
cp -r "$project_root/plugins" "$package_folder_name_absolute/usr/share/elf/"
cp -r "$project_root/scripts" "$package_folder_name_absolute/usr/share/elf/"
cp -r "$project_root/templates" "$package_folder_name_absolute/usr/share/elf/"

#Build the package
dpkg-deb --build "$package_folder_name_absolute"

# delete the unuseful folder (used to build the package)
rm -rf "$package_folder_name_absolute"
exit 0
