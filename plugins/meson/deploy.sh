#!/usr/bin/env bash

set -e

log "Deploying meson project"
meson install -C build

exit 0
