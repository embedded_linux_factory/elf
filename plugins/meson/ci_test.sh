#!/bin/bash

# Exit immediately if a command exits with a non-zero status
set -e

######################## Declaring helpers for no-docker option ########################
project_root=$(dirname "$(realpath "$0")")/../../
ci_test_dir="./ci_plugin_meson_tmp/"

######################## Testing meson plugin in docker ########################
rm -rf $ci_test_dir
mkdir $ci_test_dir
cd $ci_test_dir
cp "$project_root/plugins/meson/default/manifest_example.xml" .manifest.xml
cp "$project_root/plugins/meson/default/meson.build" meson.build
cp -r "$project_root/plugins/meson/tests" tests
"$project_root/elfrun" build.sh
"$project_root/elfrun" start-test.sh

######################## Testing meson plugin outside docker ########################
rm -rf build/

{
    echo 'export ELF_MANIFEST=".manifest.xml"'
    echo 'export -f log'
    echo "$project_root/plugins/meson/build.sh"
    echo "$project_root/plugins/meson/test.sh"
} > tmp.sh

chmod +x tmp.sh

"$project_root"/elfrun --no-docker ./tmp.sh

rm tmp.sh

cd -

exit 0
