#!/bin/bash
# Exit immediately if a command exits with a non-zero status
set -e

############################################################
# Global variables
############################################################
PKG_MAINTAINERS=""
PKG_VERSION=""
PKG_DESCRIPTION=""


# Path of this script
PKG_BUILDER_PATH=$(dirname "$(realpath "$0")")
readonly PKG_BUILDER_PATH

# Path of the project root that will be packaged
PROJECT_ROOT=$(realpath ".")
readonly PROJECT_ROOT

# Path of the debian/ folder
DEBIAN_FOLDER="$PROJECT_ROOT/debian"
readonly DEBIAN_FOLDER

# Architecture for the package
if [ -z "$ARCHITECTURE" ]; then
	ARCHITECTURE=$(dpkg --print-architecture)
	readonly ARCHITECTURE
fi

# Name of the project based on the repo name
PROJECT_NAME=$(basename "$PROJECT_ROOT")
readonly PROJECT_NAME

# Name of the output folder for the package
if [ -z "$OUTPUT_FOLDER" ]; then
	OUTPUT_FOLDER="$PKG_BUILDER_PATH/output"
	readonly OUTPUT_FOLDER
fi

PKG_NAME=""

# Get the dependencies from the manifest
DEPENDENCIES=$("$PKG_BUILDER_PATH"/../../scripts/manifest.py --manifest "$PROJECT_ROOT"/.manifest.xml docker --tag package --basic-output | xargs)
# This will add a coma before any space too match the
# control file's Depends field syntax
DEPENDENCIES="${DEPENDENCIES// /, }"
readonly DEPENDENCIES

log "$(printf "\n#####    Debian package builder for project:: %s    #####" "$PROJECT_NAME")"

############################################################
# Usage
############################################################
Usage() {
	printf "\nUsage:\n"
	printf "  %s -v <version> -m <maintainers>...\n" "$(basename "$0")"
	printf "\nOptions:\n"
	printf "  -v <version>       Version of the Debian package (format: x.x.x)\n"
	printf "  -m <maintainers>   List of maintainers in the format: \"Firstname Lastname <email>\"\n"
	printf "  -d <description>   Description of the package\n"
	printf "  -h                 Display this help\n"
	printf "\nExamples:\n"
	printf "  %s -v 1.0.0 -m \"Toto Titi <to.ti@example.com>, Linus Torvald <lili.toto@example.com>\" -d \"Some description...\"\n" "$(basename "$0")"
	printf "\n"
}

############################################################
# Args check
############################################################

VersionArgCheck() {
	if ! [[ "$1" =~ ^(0|[1-9][0-9]{0,1})\.([0-9]|[1-9][0-9]{0,1})\.([0-9]|[1-9][0-9]{0,1})$ ]]; then
		error "Error with the format of the version: $1 (see -h for help)"
	fi
	return 0
}

# Getopts
while getopts v:m:d:h option; do
	case $option in
	v)
		if VersionArgCheck "$OPTARG"; then
			PKG_VERSION=$OPTARG
		fi
		readonly PKG_VERSION
		;;
	m)
		PKG_MAINTAINERS="$OPTARG"
		readonly PKG_MAINTAINERS
		;;
	d)
		PKG_DESCRIPTION="$OPTARG"
		readonly PKG_DESCRIPTION
		;;
	h)
		Usage
		exit 1
		;;
	*)
		Usage
		exit 1
		;;
	esac
done

if [ "$PKG_VERSION" == "" ]; then
	error "Missing version argument (see -h for help)"
elif [ "$PKG_MAINTAINERS" == "" ]; then
	error "Missing maintainers argument (see -h for help)"
elif [ "$PKG_DESCRIPTION" == "" ]; then
	error "Missing description argument (see -h for help)"
fi

# Check if the version provided match the one in the
# changelog file
if [ "$(dpkg-parsechangelog -l ./CHANGELOG.md -S Version)" != "$PKG_VERSION" ]; then
	error "$(printf "The version of the package in the changelog file : %s is not the same as the one provided : %s" "$(dpkg-parsechangelog -l ./CHANGELOG.md -S Version)" "$PKG_VERSION")"
fi

if [ -z "$PKG_NAME" ]; then
	PKG_NAME="${PROJECT_NAME}_${PKG_VERSION}_${ARCHITECTURE}"
	readonly PKG_NAME
fi

readonly PKG_NAME

log "Version : $PKG_VERSION"
log "Maintainers : $PKG_MAINTAINERS"
log "Description : $PKG_DESCRIPTION"
log "Dependencies : $DEPENDENCIES"

############################################################
# Creation of mandatory files to build the package
############################################################

if [ ! -d "$DEBIAN_FOLDER" ]; then
	mkdir "$DEBIAN_FOLDER"
fi

# Control file generation
cat <<EOF >"$DEBIAN_FOLDER/control"
Source: $PROJECT_NAME
Section: devel
Priority: optional
Maintainer: $PKG_MAINTAINERS
Build-Depends: debhelper (>= 12), meson, ninja-build

Package: $PROJECT_NAME
Architecture: any
Essential: no
Depends: $DEPENDENCIES
Description: $PKG_DESCRIPTION
EOF

# Rules file generation :
# dpkg-buildpackage uses the debian/rules file to know how
# to build the sources, from the sources to the install
# It expects tree main targets: build, clean and install.
# Here, as we use meson to build the project, we make use of
# the debhelper tool that already knows how to handle meson
# projects. We override some steps to specify some option
# such as meson, the builddir, the output folder...
cat <<EOF >"$DEBIAN_FOLDER/rules"
#!/usr/bin/make -f

# Export necessary environment variables
export DH_VERBOSE=1
export DEB_BUILD_MAINT_OPTIONS = hardening=+all
export DEB_BUILD_OPTIONS += noddebs

# Include the debhelper makefile
%:
	dh \$@

# Override the build step to use Meson
override_dh_auto_configure:
	dh_auto_configure --buildsystem=meson+ninja --builddir=build

override_dh_auto_build:
	dh_auto_build --buildsystem=meson+ninja --builddir=build

override_dh_auto_install:
	dh_auto_install --buildsystem=meson+ninja --builddir=build

override_dh_shlibdeps:
	dh_shlibdeps --dpkg-shlibdeps-params=--ignore-missing-info

override_dh_builddeb:
	mkdir -p $OUTPUT_FOLDER
	dh_builddeb --destdir=$OUTPUT_FOLDER --filename=$PKG_NAME.deb
EOF

# Make the rules file executable
chmod +x "$DEBIAN_FOLDER/rules"

# Changelog file creation
cp "$PROJECT_ROOT/CHANGELOG.md" "$DEBIAN_FOLDER/changelog"

# License file creation based on the project's license
cp "$PROJECT_ROOT/LICENSE" "$DEBIAN_FOLDER/copyright"

# README file creation based on the project's README
cp "$PROJECT_ROOT/README.md" "$DEBIAN_FOLDER/README.Debian"

# Compat file creation
echo "13" >"$DEBIAN_FOLDER/compat"

############################################################
# Creation of the debian Package
#	dpkg-buildpackage is a sort of wrapper around several
#	other tools, including dpkg-source, debhelper...
#   All the files created above (placed in debian/) are used
# 	to specifiy how the package should be built.
############################################################
dpkg-buildpackage --build=binary -us -uc \
	--buildinfo-option=-u"$OUTPUT_FOLDER" \
	--buildinfo-option=--always-include-kernel \
	--buildinfo-file="$OUTPUT_FOLDER"/"$PKG_NAME".buildinfo \
	--changes-option=-u"$OUTPUT_FOLDER" \
	--changes-file="$OUTPUT_FOLDER"/"$PKG_NAME".changes \
	--sanitize-env --post-clean

exit 0
