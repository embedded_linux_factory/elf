#!/usr/bin/env bash
set -e

######################## Declaring global variables ########################
# Set the manifest file path using a CI environment variable or relative path
MANIFEST_PY="$(dirname "$(realpath "$0")")/../../scripts/manifest.py"

readonly MANIFEST_PY

######################## Retrieve Valgrind Script from Manifest ########################
VALGRIND_SCRIPT=$($MANIFEST_PY --manifest="$ELF_MANIFEST" meson --tag valgrind_script --basic-output)

######################## Executing ########################
mkdir -p installdirfortest
DESTDIR="$PWD/installdirfortest" meson install -C build
PATH="$PWD/installdir/usr/local/bin/:$PATH"
meson test -C build

exit 0
