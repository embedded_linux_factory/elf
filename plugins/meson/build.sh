#!/usr/bin/env bash

set -e

######################## Declaring global variable ########################
MANIFEST_PY="$(dirname "$(realpath "$0")")/../../scripts/manifest.py"
readonly MANIFEST_PY

######################## Arguments ########################
setup_args=$(${MANIFEST_PY} --manifest="$ELF_MANIFEST" meson --tag setup --basic-output)

######################## Executing ########################
log "Building meson project"
mkdir -p build
meson setup build "$setup_args"
meson compile -C build

exit 0
