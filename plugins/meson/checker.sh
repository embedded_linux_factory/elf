#!/usr/bin/env bash

# This script is used to check 3 things:
# - If all the C files (.c, .h) are formatted correctly
# - If the license header is present in all the required
#   files
# - If all bash scripts are formatted correctly

############################################################
# Help
############################################################
Help() {
    printf "\nUsage:\n"
    printf "  %s [-h]\n" "$(basename "$0")"
    printf "\nOptions:\n"
    printf "  -h          Display this help\n"
    printf "\nExamples:\n"
    printf "  <ELF_PATH>/elfrun -- %s [-h]\n" "$(basename "$0")"
    printf "\n"
}

############################################################
# Global variables
############################################################
RETURN_CODE=0
NOK_FMT_FILES=""
NOK_LC_FILES=""

PROJECT_ROOT=$(realpath ".")
readonly PROJECT_ROOT

############################################################
# Options Handling
############################################################
while getopts "hv" option; do
    case $option in
    h) # display Help
        Help
        exit
        ;;
    *)
        Help
        exit
        ;;
    esac
done

############################################################
# Files finding
############################################################

# Get all the files that are not ignored by the .gitignore's
# rules (because they are non-build related etc)
GIT_NOT_IGNORED_FILES=$(git ls-files)

for nignored_file in $GIT_NOT_IGNORED_FILES; do
    if [[ $nignored_file == *.c ]] || [[ $nignored_file == *.h ]]; then
        C_FILES="$C_FILES ./$nignored_file"
    elif [[ $nignored_file == *.sh ]] || [[ $nignored_file == *meson.build ]]; then
        LCS_FILES="$LCS_FILES ./$nignored_file"
    fi
done
# LCS_FILES also need to check C files
LCS_FILES="$LCS_FILES $C_FILES"

############################################################
# FORMAT CHECKER
############################################################

# This will check if all the C files (.c, .h) are formatted
# correctly with the clang-format config of this project

# Check if clang-format is installed
if ! command -v clang-format >/dev/null 2>&1; then
    error "clang-format could not be found"
fi

log "Checking format of C files...\n"
for file in $C_FILES; do
    if ! clang-format "$file" | cmp -s "$file"; then
        log "   $file... NOK" 
        NOK_FMT_FILES="$NOK_FMT_FILES$file "
    else
        log "   $file... OK"
    fi
done

############################################################
# LICENSE CHECKER
############################################################

# This will check if all the required files have the correct
# license header

log "Checking license in files..."
for file in $LCS_FILES; do
    if ! grep -q "SPDX-License-Identifier: GPL-2.0-only" "$file"; then
        log "   $file... NOK"
        NOK_LC_FILES="$NOK_LC_FILES$file "
    else
        log "   $file... OK\n"
    fi
done

############################################################

if [ -n "$NOK_FMT_FILES" ]; then
    warn "The following files need to be correctly formatted:"
    for file in $NOK_FMT_FILES; do
        warn "  $file"
    done
    RETURN_CODE=1
fi

if [ -n "$NOK_LC_FILES" ]; then
    warn "The following files need to have a correct license header:"
    for file in $NOK_LC_FILES; do
        warn "  $file"
    done
    RETURN_CODE=1
fi

############################################################
# BASH CHECKER
############################################################
"$(dirname "$0")"/../yocto/checker.sh -p "$PROJECT_ROOT"

############################################################

RETURN_CODE=$(( "$RETURN_CODE" || "$?" ))

exit $RETURN_CODE
