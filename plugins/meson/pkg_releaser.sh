#!/bin/bash

# Exit immediately if a command exits with a non-zero status
set -e

# The package releaser script is intended to be use in the
# gitlab CI.
#   - It will first create the debian package with the right
#     version and the right maintainer list and description
#     using the pkg_builder.sh script.
#   - Then it will upload the .deb using the gitlab debian
#     API
#   - Then it will create the release note of this new
#     version using the tool release-cli by gitlab

############################################################
# Global variables
############################################################
PKG_MAINTAINERS=""
PKG_VERSION=""
PKG_DESCRIPTION=""

ELF_PATH=$(realpath -mL "$0/../../..")
readonly ELF_PATH

# Name of the project based on the repo name
PROJECT_NAME=$(basename "$(realpath ".")")
readonly PROJECT_NAME

# OUTPUT_FOLDER
OUTPUT_FOLDER="$ELF_PATH/plugins/meson/output"
readonly OUTPUT_FOLDER

ARCHITECTURE=$(dpkg --print-architecture)
readonly ARCHITECTURE

log "$(printf "\n#####    Creating Gitlab release for project: %s    #####" "$PROJECT_NAME")"
############################################################
# Usage
############################################################
Usage() {
    printf "\nUsage:\n"
    printf "  %s [options]\n" "$(basename "$0")"
    printf "\nOptions:\n"
    printf "  -v <version>       Version of the Debian package (format: x.x.x)\n"
    printf "  -m <maintainers>   List of maintainers in the format: \"Firstname Lastname <email>, Firstname2 Lastname2...\"\n"
    printf "  -d <description>   Description of the package\n"
    printf "  -h                      Display this help message\n"
    printf "\nMandatory environment variables:\n"
    printf "  -CI_JOB_TOKEN            Gitlab CI job token\n"
    printf "  -PACKAGE_REGISTRY_URL    URL of the package registry, must be created in the CI\n"
    printf "  -CI_COMMIT_TAG           Git tag of the commit\n"
    printf "  -CI_COMMIT_SHA           Git commit sha\n"
    printf "  -CI_SERVER_URL           Gitlab server URL\n"
    printf "  -CI_PROJECT_ID           Gitlab project ID\n"
    printf "\nExample:\n"
    printf "  %s -v 0.0.1 -m \"Toto Titi <to.ti@example.com>, Linus Torvald <lili.toto@example.com>\" -d "Some description..."\n" "$(basename "$0")"
    printf "\n"
}

############################################################
# Args check
############################################################

while getopts v:m:d:j:h option; do
    case $option in
    v)
        PKG_VERSION=$OPTARG
        readonly PKG_VERSION
        ;;
    m)
        PKG_MAINTAINERS="$OPTARG"
        readonly PKG_MAINTAINERS
        ;;
    d)
        PKG_DESCRIPTION="$OPTARG"
        readonly PKG_DESCRIPTION
        ;;
    h)
        Usage
        exit 1
        ;;
    *)
        Usage
        exit 1
        ;;
    esac
done

# Check mandatory parameters
if [ -z "$PKG_VERSION" ]; then
    error "Missing version argument (see -h for help)"
elif [ -z "$PKG_MAINTAINERS" ]; then
    error "Missing maintainers argument (see -h for help)"
elif [ -z "$PKG_DESCRIPTION" ]; then
    error "Missing description argument (see -h for help)"
fi

# Check mandatory environment variables
# CI_JOB_TOKEN is special. It is a secret variable that is
# when evaluate return an empty string and when printed
# return the string "[MASKED]".
if [ -z "$(printf "%s" "$CI_JOB_TOKEN")" ] || [ -z "$PACKAGE_REGISTRY_URL" ] || [ -z "$CI_COMMIT_TAG" ] || [ -z "$CI_COMMIT_SHA" ] || [ -z "$CI_SERVER_URL" ] || [ -z "$CI_PROJECT_ID" ]; then
    error "One or more required environment variables are missing: CI_JOB_TOKEN, PACKAGE_REGISTRY_URL, CI_COMMIT_TAG, CI_COMMIT_SHA, CI_SERVER_URL, CI_PROJECT_ID"
    exit 1
fi

PKG_NAME="${PROJECT_NAME}_${PKG_VERSION}_${ARCHITECTURE}"
readonly PKG_NAME

PKG_URL="$PACKAGE_REGISTRY_URL/$PKG_VERSION/$PKG_NAME"
PKG_DEB_URL="$PACKAGE_REGISTRY_URL/$PKG_VERSION/$PKG_NAME.deb"
PKG_BUILDINFO_URL="$PACKAGE_REGISTRY_URL/$PKG_VERSION/$PKG_NAME.buildinfo"
PKG_CHANGES_URL="$PACKAGE_REGISTRY_URL/$PKG_VERSION/$PKG_NAME.changes"
readonly PKG_URL
readonly PKG_DEB_URL
readonly PKG_BUILDINFO_URL
readonly PKG_CHANGES_URL

############################################################
# Create .deb in output/
############################################################
log "$(printf "\nCreating debian package for project: %s with version: %s" "$PROJECT_NAME" "$PKG_VERSION")"
"$ELF_PATH"/plugins/meson/pkg_builder.sh -v "$PKG_VERSION" -m "$PKG_MAINTAINERS" -d "$PKG_DESCRIPTION"

############################################################
# Upload .deb, .buildinfo and .changes to the registry
############################################################
log "$(printf "\nUploading debian package: %s to the registry :" "$PKG_NAME.deb")"
log "   - $PKG_DEB_URL"
curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file "$(realpath "$OUTPUT_FOLDER/$PKG_NAME.deb")" "$PKG_DEB_URL"

log "$(printf "\nUploading the buildinfo: %s to the registry :" "$PKG_NAME.buildinfo")"
log "   - $PKG_BUILDINFO_URL"
curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file "$(realpath "$OUTPUT_FOLDER/$PKG_NAME.buildinfo")" "$PKG_BUILDINFO_URL"

log "$(printf "\nUploading the changes: %s to the registry :" "$PKG_NAME.changes")"
log "   - $PKG_CHANGES_URL"
curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file "$(realpath "$OUTPUT_FOLDER/$PKG_NAME.changes")" "$PKG_CHANGES_URL"

############################################################
# Create release note
############################################################
log "Creating release note..."

if [ "$(dpkg-parsechangelog -S Version)" != "$PKG_VERSION" ]; then
    error "$(printf "The version of the package : %s is not the same as the one provided : %s" "$(dpkg-parsechangelog -S Version)" "$PKG_VERSION")"
fi

#Parse the changelog file to extract the changes
dpkg-parsechangelog -S Changes >>changes.txt

#Create the release on gitlab using the API
release-cli create --name "$CI_COMMIT_TAG" \
    --description changes.txt \
    --tag-name "$CI_COMMIT_TAG" \
    --ref "$CI_COMMIT_SHA" \
    --assets-link "[{\"name\":\"Debian package\",\"url\":\"$PKG_DEB_URL\"}, \
                    {\"name\":\"Debian package .buildinfo\",\"url\":\"$PKG_BUILDINFO_URL\"}, \
                    {\"name\":\"Debian package .changes\",\"url\":\"$PKG_CHANGES_URL\"}]"

log "$(printf "\nRelease note created for version: %s" "$PKG_VERSION")"

rm changes.txt
exit 0
