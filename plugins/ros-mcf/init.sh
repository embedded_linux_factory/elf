#!/usr/bin/env bash

set -e

echo "#### init ros ####"

####################################################
mkdir -p conf
ln -snf ../conf build/.

ROS_VERSION=$(manifest.py plugin[@name=\'"ros-mcf"\'] --tag distro --basic-output)

if [[ ! -f "build/files/$ROS_VERSION.mcf" ]]; then
    echo "Version $ROS_VERSION doesn't exist"
    find . -maxdepth 3 -path "*plugins/yocto/*" -exec basename {} \; | cut -d"." -f1
    exit 1
fi
cp "build/files/$ROS_VERSION.mcf" conf/.

build/scripts/mcf -f "conf/$ROS_VERSION.mcf"
####################################################

if [[ -z "$OE_INIT_ENV_PATH" ]]; then
    echo "Variable OE_INIT_ENV_PATH must be set in Dockerfile"
    exit 1
fi

echo "#####     Init Yocto environment $YOCTO_CONFIG    #####"
source openembedded-core/oe-init-build-env
