SUMMARY = "ROS2 C++ PACKAGE recipe"
DESCRIPTION = "Recipe to import C++ packages in meta-ros"
LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://LICENSE;md5=3b83ef96387f14655fc854ddc3c6bd57"

SRC_URI = "file://YOUR SOURCES/"
S="${WORKDIR}/LICENSE FILE LOCATION"

inherit ros_distro_humble
inherit ros_superflore_generated

ROS_CN = "PACKAGE_NAME(I THINK)"
ROS_BPN = "PACKAGE_NAME"

ROS_BUILD_TYPE = "ament_cmake"


ROS_BUILD_DEPENDS += " \
    THE RECIPES OF THE PACKAGE DEPEDENCIES
"

ROS_BUILDTOOL_DEPENDS += " \
    ament-cmake-native \
"

ROS_EXPORT_DEPENDS += " \
    THE RECIPES OF THE PACKAGE DEPEDENCIES \
"

ROS_BUILDTOOL_EXPORT_DEPENDS += ""

ROS_EXEC_DEPENDS += " \
    THE RECIPES OF THE PACKAGE DEPEDENCIES \
"

# Currently informational only -- see http://www.ros.org/reps/rep-0149.html#dependency-tags.
ROS_TEST_DEPENDS += " \
    ament-lint-auto \
    ament-lint-common \
"

DEPENDS += "${ROS_BUILD_DEPENDS} ${ROS_BUILDTOOL_DEPENDS}"
# Bitbake doesn't support the "export" concept, so build them as if we needed them to build this package (even though we actually
# don't) so that they're guaranteed to have been staged should this package appear in another's DEPENDS.
DEPENDS += "${ROS_EXPORT_DEPENDS} ${ROS_BUILDTOOL_EXPORT_DEPENDS}"

RDEPENDS:${PN} += "${ROS_EXEC_DEPENDS}"


inherit ros_${ROS_BUILD_TYPE}
