#!/usr/bin/env bash

set -e

if [[ ! -d "build" ]]; then
    git clone -b "build" --single-branch https://github.com/ros/meta-ros.git "build"
fi

exit 0
