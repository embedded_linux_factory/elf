#!/usr/bin/env bash

set -e

######################## Declaring global variable ########################
MANIFEST_PY="$(dirname "$(realpath "$0")")/../../scripts/manifest.py"
VERSION="$($MANIFEST_PY --manifest="$ELF_MANIFEST" plugin[@name=\'"yocto"\'] --tag version --basic-output)"

readonly MANIFEST_PY
readonly VERSION

######################## Executing ########################
IMAGE="$($MANIFEST_PY --manifest="$ELF_MANIFEST" config[@name=\'"$ELF_ENV_CONFIG"\']/image --tag name --basic-output)"
if [[ -z "$IMAGE" ]]; then
    IMAGE="core-image-minimal"
fi

log "[$VERSION] Building $IMAGE for config $ELF_ENV_CONFIG "
bitbake -k "$IMAGE"

exit 0
