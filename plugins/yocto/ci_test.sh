#!/bin/bash

# Exit immediately if a command exits with a non-zero status
set -e

project_root=$(dirname "$(realpath "$0")")/../../
ci_test_dir="./ci_plugin_yocto_tmp/"

rm -rf $ci_test_dir
mkdir $ci_test_dir
cd $ci_test_dir

# Clean older elf files
rm -rf ~/.elf/"$PWD"

cp "$project_root/plugins/yocto/scarthgap/manifest_example.xml" .manifest.xml
"$project_root/elfrun" build.sh
cd -

exit 0
