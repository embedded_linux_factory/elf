#!/usr/bin/env bash

set -e

######################## Declaring global variable ########################
ELFDIR="$(dirname "$(realpath "$0")")/../.."
WORKSPACE_PATH=$PWD
YOCTO_CONFIG="$1"
ELF_INTERACTIVE_MODE="$2"
: "${config:="no_config"}"

if [[ -n "$ELF_MANIFEST" ]]; then
    MANIFEST_PATH=$(basename "$ELF_MANIFEST")
else
    MANIFEST_PATH=".manifest.xml"
fi

readonly ELFDIR
readonly WORKSPACE_PATH
readonly YOCTO_CONFIG
readonly ELF_INTERACTIVE_MODE
readonly MANIFEST_PATH

######################## Executing ########################
# Variable checking
[[ -n "$YOCTO_CONFIG" ]] || error "Unknown configuration: [$config]"
[[ -n "$OE_INIT_ENV_PATH" ]] || error "Variable OE_INIT_ENV_PATH must be set in Dockerfile"

log "#####     Init Yocto environment $YOCTO_CONFIG    #####"
mkdir -p "build/$YOCTO_CONFIG"
source "$OE_INIT_ENV_PATH" "build/$YOCTO_CONFIG" >/dev/null

cp "$ELFDIR/plugins/yocto/$YOCTO_BRANCH/templateconf/local.conf.sample" "conf/local.conf"

# Check personal Yocto max cache size
if [[ -d "/share" ]]; then
    ELF_ENV_CONFIG_MANIFEST="/share/home/$(whoami)/.elf/env-conf.xml"
else
    ELF_ENV_CONFIG_MANIFEST="/home/$(whoami)/.elf/env-conf.xml"
fi

if [[ ! -f "$ELF_ENV_CONFIG_MANIFEST" ]]; then
    mkdir -p "$(dirname "$ELF_ENV_CONFIG_MANIFEST")"
    cp "$ELFDIR/plugins/yocto/$YOCTO_BRANCH/templateconf/env-conf.xml.sample" "$ELF_ENV_CONFIG_MANIFEST"
fi
cache_dir=$("$ELFDIR/scripts/manifest.py" --manifest="../../$MANIFEST_PATH" config[@name=\'"$YOCTO_CONFIG"\']/project-var project-var --tag SSTATE_DIR --basic-output)
if [[ -d "$cache_dir" ]]; then
    cache_dir_size=$(du -sh "$cache_dir" | awk '{print $1}')
    if [[ $cache_dir_size =~ G$ ]]; then
        cache_dir_size=${cache_dir_size//,/./}
        cache_size=${cache_dir_size//[KMG]/}
        cache_size_int=$(awk "BEGIN {print int($cache_size)}")

        limit_size=$("$ELFDIR"/scripts/manifest.py --manifest="$ELF_ENV_CONFIG_MANIFEST" yocto --tag cache_max_size --basic-output)
        limit="${limit_size//[KMG]/}"
        if [[ "$cache_size_int" -gt "$limit" ]]; then
            echo "$cache_dir directory is too voluminous ($cache_dir_size > $limit_size max desired), cleaning up..."
            if [[ -n $ELF_INTERACTIVE_MODE ]]; then
                ../../layers/poky/scripts/sstate-cache-management.py --remove-duplicated --cache-dir="$cache_dir"
            else
                ../../layers/poky/scripts/sstate-cache-management.py --remove-duplicated --cache-dir="$cache_dir" --yes
            fi
        fi
    fi
fi

PROJECT_VARS=$("$ELFDIR/scripts/manifest.py" --manifest="../../$MANIFEST_PATH" config[@name=\'"$YOCTO_CONFIG"\']/project-var project-var --basic-output | sed 's/^project-var.//')

declare -A attributes_dict
for attribute in $PROJECT_VARS; do
    IFS=" " read -r -a attr_array <<<"${attribute//\:\:/ }"
    attributes_dict["${attr_array[0]}"]="${attr_array[1]}"
done

PROJECT_CONF="conf/project.conf"
printf "" >"$PROJECT_CONF"
for vars in "${!attributes_dict[@]}"; do
    echo "${vars}=\"${attributes_dict[$vars]}\"" >>"$PROJECT_CONF"
done

{
    echo "PROJECT_NAME=\"${PROJECT_NAME}\""
    echo "PROJECT_VERSION=\"${PROJECT_VERSION}\""
    echo "PROJECT_CHECKSUM=\"${PROJECT_CHECKSUM}\""
} >>$PROJECT_CONF

bbmask=$("$ELFDIR/scripts/manifest.py" --manifest="../../$MANIFEST_PATH" config[@name=\'"$YOCTO_CONFIG"\']/bbmask bbmask --tag path --basic-output)
for mask in $bbmask; do
    echo "BBMASK += \"$mask\"" >>$PROJECT_CONF
done

include_conf=$("$ELFDIR/scripts/manifest.py" --manifest="../../$MANIFEST_PATH" config[@name=\'"$YOCTO_CONFIG"\']/include_conf include_conf --tag path --basic-output)
for f in $include_conf; do
    filename=$(basename "$f")
    cp -f "../../${f}" "conf/${filename}"
    echo "include conf/${filename}" >>$PROJECT_CONF
done

for feature in $("$ELFDIR/scripts/manifest.py" --manifest="../../$MANIFEST_PATH" config[@name=\'"$YOCTO_CONFIG"\']/yocto yocto --tag features --basic-output); do
    if [[ -f "$ELFDIR/default/yocto/features/$feature.conf" ]]; then
        if [[ -f "conf/${feature}.conf" ]]; then
            warn "Cannot add feature ${feature} because conf/${feature}.conf exists"
        else
            cp -f "$ELFDIR/default/yocto/features/$feature.conf" "conf/$feature.conf"
            echo "include conf/$feature.conf" >>"$PROJECT_CONF"
        fi
    fi
done
# add a layer if it is needed and doesn't exist in bblayers.conf
layers=()
for layer in $("$ELFDIR/scripts/manifest.py" --manifest="../../$MANIFEST_PATH" config[@name=\'"$YOCTO_CONFIG"\']/layer layer --tag path --basic-output); do
    grep -q "$layer" conf/bblayers.conf || layers+=("$WORKSPACE_PATH/$layer")
done

[[ -z "$(echo "${layers[*]}" | xargs)" ]] || bitbake-layers add-layer "${layers[@]}"
