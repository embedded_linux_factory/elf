#!/usr/bin/env bash

set -e

# layers does not exist -> create it
[[ -d "layers/" ]] || mkdir -p "layers/"

# Poky does not exist -> create it
if [[ ! -d "layers/poky" ]]; then
    echo "Warning : you don't specify Poky layer, it will fetch the latest revision depending on your branch.
    See layers/revisions.txt."

    git clone --depth=1 https://git.yoctoproject.org/poky -b "${YOCTO_BRANCH}" layers/poky
fi

exit 0
