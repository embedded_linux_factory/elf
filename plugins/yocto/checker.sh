#!/usr/bin/env bash
#
# This script is used to check :
# - If all the bash scripts (.sh, no extension) are formatted correctly

set -eu

###############################################################################
# Help
###############################################################################
Help() {
    echo ""
    echo "Usage:"
    echo "  $(basename "$0") [-p <project_root>] [-h]"
    echo ""
    echo "Options:"
    echo "  -h          Display this help"
    echo "  -p          Path to the project root."
    echo "              If not specified, we use ../.. (for now) as default to match yocto project structure."
    echo ""
    echo "Examples:"
    echo "  <ELF_PATH>/elfrun -- $(basename "$0") -p /project/dir" 
    echo ""
}

###############################################################################
# Global variables
###############################################################################
RETURN_CODE=0
NOK_FMT_FILES=""
SH_FILES=""

PROJECT_ROOT="$(realpath ../..)"

###############################################################################
# Options Handling
###############################################################################
while getopts "hp:" option; do
    case $option in
    p)
        PROJECT_ROOT="$OPTARG"
        ;;
    h)
        Help
        exit
        ;;
    *)
        Help
        exit
        ;;
    esac
done

###############################################################################
# Files finding
###############################################################################

cd "$PROJECT_ROOT"

# Get all the files that are not ignored by the .gitignore's
# rules (because they are non-build related etc)
GIT_NOT_IGNORED_FILES=$(git ls-files)

for nignored_file in $GIT_NOT_IGNORED_FILES; do
    # Keep bash scripts (ex /bin/bash, /bin/sh, /bin/ksh), even if they are no
    # .sh extension.
    # Licensed files define shebang in the 2nd line
    if [[ $(head -n 2 "$nignored_file" | grep -E '^#!.*(bash|sh|ksh)') ]]; then
        SH_FILES+="$nignored_file "
    fi
done

###############################################################################
# SHELL CHECKER
###############################################################################

# This will check if all project bash scripts respects
# the shellcheck rules

# Check if shellcheck is installed
if [[ ! $(command -v shellcheck) ]]; then
    error "shellcheck could not be found."
fi

log "Checking format of bash scripts..."
for file in $SH_FILES; do
    if [[ $(shellcheck "$file") ]]; then
        log "   $file... NOK"
        NOK_FMT_FILES+="$file "
    else
        log "   $file... OK"
    fi
done

cd - >/dev/null 2>&1

###############################################################################

if [[ -n "$NOK_FMT_FILES" ]]; then
    warn "The following files need to pass shellcheck:"
    for file in $NOK_FMT_FILES; do
        warn "  $file"
    done
    warn "Please use shellcheck and fix this."
    RETURN_CODE=1
fi

exit $RETURN_CODE
