# Yocto plugin

This plugin aim at facilitating reproducible Yocto build

## Manifest specific format

### project-var (Optional)

Variable to set in build/<config>/conf/project.conf
This configuration file is added to the default local.conf

### layer (Optional)

Project type specific label to define what layers is used by that configuration.

### deploy (Optional)

Information to deploy images on that machine.

#### image

Name of default image to build

### include_conf

Path to a configuration file to include in local.conf
