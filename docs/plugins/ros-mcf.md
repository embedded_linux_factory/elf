# ROS-mcf plugin

This plugin aim at facilitating reproducible ROS build.

## Manifest specific format

To leverage the ROS-mcf plugin in your project, you need to include a specific line in your manifest. Add the following line:

```xml
<plugin name="ros-mcf" version="<plugin_version>" distro="<ros_distribution>"/>
```

with :
- `<plugin_version>` : Specify the version of the ROS-mcf plugin you want to use.
- `<ros_distribution>` : Define the ROS distribution you intend to work with. Note that this refers to a version of meta-ros. The parameter <ros_distribution> includes both the ROS distribution (e.g., "ros2-humble") and the version of OpenEmbedded (e.g., "kirkstone").

For example, to use the ROS 2 distribution named "ros2-humble-kirkstone" with the the version 0.0.1 of the ROS plugin, include the following line:
```xml
<plugin name="ros-mcf" version="0.0.1" distro="ros2-humble-kirkstone"/>
```
You can find an example of this application in the file "elf/plugins/ros-mcf/0.0.1/.manifest_example.xml".