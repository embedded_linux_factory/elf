# Meson plugin

This plugin aim at facilitating reproducible meson build

## Manifest specific format

### setup

This flag is used to pass addtional arguments to meson setup

### valgrind_script

Used to define the path to a script to pass valgrind on
binaries

## Checker script
There is a script available when using this plugin to check
the sources files.  
It will check if the C files are correctly formatted
according to the current clang-format rules of the project.
It will also check if all the source files have the correct 
license header.

**Usage:**
```
  checker.sh [-v|h]

Options:
  -v          Verbose mode
  -h          Display this help

Examples:
  <ELF_PATH>/elfrun "checker.sh [-v|h]"
```

## Debian Package Builders script
There is a script available when using this plugin to build
a Debian package from a meson project.

**Usage:**
```
  package_builder.sh -v <version> -m <maintainers>...

Options:
  -v <version>       Version of the Debian package (format: x.x.x)
  -m <maintainers>   List of maintainers in the format: "Firstname Lastname <email>"

Examples:
  package_builder.sh -v 1.0.0 -m "Toto Titi <to.ti@example.com>" -m "Linus Torvald <lili.toto@example.com>"
```


