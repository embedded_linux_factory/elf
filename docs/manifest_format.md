# Manifest format


## manifest

This label is mandatory and unique to encapsulate datas.

### version

Specify the compatibility version number of the manifest.
This is useful to ensure that ELF support that manifest version.


## default

Define default informations

### name

Define the name of the project.
This value is exported as a PROEJCT_NAME in the docker environment

### version

Define the version name of the project.
This value is exported as a PROEJCT_VERSION in the docker environment


## plugin

Specify that a plugin have to be used to provide more functionnalities

See pligins/plugin.md for more information


## hosts

Allow to set specific value in /etc/hosts files

#### value

Specify the content to write in /etc/hosts.
The format of value must be "ip1 name1,ip2 name2,..."
```
<hosts value="192.168.1.12 monserveur />
```


## remote

As for google repo, this label is used to define a remote server containing sources;

### name

Name of the remote used as a reference by other labels

### fetch

Url of the remote


## project

Repository to clone in order to get sources

### name

name of the repository on the remote server

### remote

Reference to the remote url

### path

Destination where to clone the repository

### revision (Optional)

Revision to use for that repository

### branch (Optional)

Branch to use with that repository if revision is not set


## config (Optional)

Define a specific configuration to set. This can be useful for various plugins

## docker (optional)

A way to customize or overload docker behaviour from the manifest

### package

This tag allow to install additionnal package to the final project image

### publish_port

This allow to publish container port and access to it using network.
The format of this value should follow -p option of docker.

https://docs.docker.com/get-started/docker-concepts/running-containers/publishing-ports/

### capabilities

Allow adding capabilities to the container.

See : https://dockerlabs.collabnix.com/advanced/security/capabilities/


## test (optional)

This item allow to specify test behaviour

### command

This tag allows to specify the command to execute when running:
- 'elfrun test.sh' outside of a container
- 'test.sh' inside a container


## fetch

This element makes it possible to get sources from different
ways specify by the `type` attribute. 

#### - `type` attributes :  
The type attribute can be one of the following values :
- `git` : fetch sources from a git repository (clone the
  repository at a specific location)
- `tar` : fetch sources from a tarball (decompress the
  tarball at a specific location)
- `local` : fetch sources from a local directory (copy the
  content of the directory at a specific location)
- `deb` : install a .deb package in the container

Each type has its own attributes to specify the way to fetch
sources :

- `git` : 
    - `name` : name of the project to clone
    - `path` : path where to clone the repository
    - `url` : url of the git repository
    - `branch` : branch to clone (optional).
    - `sha1-rev` : SHA1 revision to checkout (optional).

- `tar` :
    - `path` : path where to decompress the tarball
    - `url` : url of the tarball (either a file or an http
      url)

- `local` :
    - `path` : path where to copy the content of the local
      directory
    - `local_path` : path of the local directory

- `deb` :
    - `url` : url of the .deb package to download and to install

#### Exemples :

```xml
<fetch type="git" name="toto_application" path="path/to/clone" url="https://git.yoctoproject.org/git" sha1-rev="a1b2c3d4"/>

<fetch type="tar" path="path/to/decompress" url="http://example.com/tarball.tar"/>

<fetch type="local" path="path/to/copy" local_path="/path/to/local/directory"/>

<fetch type="deb" url="FILE:///home/monitor0.0.0-1_amd64.deb"/>
```
