# manifest.py

This tool is the main tool to retrieve information from the
manifest in Python and Shell

By default, we read information in .manifest.xml, but the
file can be changed using '--manifest=<filename>'

## Vocabulary
**Element** : A tag in the XML file  
**Attribute** : A key-value pair in an element
```xml
<root_element attribute1="value1">
    <child_element1 attribute2="value2"/>
    <child_element2>
        <subchild_element1 attribute3="value3"/>
    </child_element2>
</root_element>
```


## Usage 
```
$ manifest.py <elements> [options] 
```
### Elements :
Paths of the XML elements selected to be read. We can
specify several elements separated by a space. 
#### Ex :
- The root element : **`.`**
- Other child or grand-child elements from the root :
  **`default`**, **`docker`**, **`config/fetch`** and many more ...

### Filtering :
We can also use xfilter to filter the elements by a specific
attribute's value (see
https://www.w3schools.com/xml/xpath_syntax.asp)

#### Ex :
- **`config[@name='qemux86-64']`** : the config element with
  the name attribute equals to `qemux86-64`

### Options :
- **`--manifest=path/to/a/manifest`** :
    - specify the manifest file to read from (default is 
      .manifest.xml)
- **`--tag=tagname`** :
    - For each selected element, specify a particular
      attribute value to read and return.  
      If not specified, the whole attribute list of elements
      will be returned.
- **`--basic-output`** (or `-b`):
    - Only return the value of the attribute, without the
      element name


## Examples (manifest from rws project used)
You can get an attribute and its value by its name. For
example to show manifest compatibility version (from the
root element).

```
$ manifest.py . --tag version
manifest: '1'
```

Show the name of the
project (specified in the `default` element).

```
$ manifest.py default --tag name
default: 'rws'
```

If you just want the values without the attribute, use
**`--basic-output`** option.

```
$ manifest.py default --tag name --basic-output 
rws
```

If you want to filter the elements by an attribute value,
you can do it with `xfilters`. For example to get all the
`layer` elements of a particular config (`config` elements
with its attribute `name`=`rws-rpi3` here) and return the
`path` attribute value of those elements.
    
```
$ manifest.py config[@name='rws-rpi3']/layer --tag path 
layer: 'layers/meta-raspberrypi'
```

You can also return the hole element's attributes by not
specifying the `--tag` option. With the format
`attribute1::value1 attribute2::value2 ...` 

```
$ manifest.py config[@name='qemux86-64']/project-var --basic-output 
MACHINE:qemux86-64 DISTRO:rws
```


 