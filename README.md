# Embedded linux factory

This tool aims at simplifying linux development and integration.
It simplify the use of docker to work on a project.
The user environment is duplicated inside the docker but the environment and tools are set to the project need.
It optionnaly uses a manifest file to fetch sources using git.


## Dependencies

ELF has the following dependencies:
  - docker
  - git
  - python3
  - python3-lxml

A script to setup the environment can be found at scripts/setup-elf-requirements.sh


## Usage

Enter a new empty directory or use an existing one with a .manifest.xml file.

ELF has 3 differents command:
- elfmanifest
- elfdocker
- elfrun

Warning : do not use sudo for those commands

### Simple manifest format

```xml
<?xml version="1.0" encoding="UTF-8"?>
<manifest version="1">
  <default name="elftest" version="0.0.1" />

</manifest>
```

### Yocto basic manifest format

```xml
<?xml version="1.0" encoding="UTF-8"?>
<manifest version="1">
  <default name="elftestyocto" version="0.0.1" />

  <plugin name="yocto" version="scarthgap"/>

  <remote fetch="https://git.yoctoproject.org/git" name="yocto"/>
  <project remote="yocto" name="meta-raspberrypi" path="layers/meta-raspberrypi"/>

  <config name="rpi" >
    <layer path="layers/meta-raspberrypi"/>
    <project-var MACHINE="raspberrypi3-64" DISTRO="poky" ENABLE_UART="1" />
    <image name="core-image-minimal" />
  </config>

</manifest>
```

## elfmanifest

This tool can be used to manage manifest file
```bash
$ elfmanifest -h
usage: elfmanifest [-h] [-c] [-i INPUTFILE] [-n] [-o OUTPUTFILE] [-p PROJECTNAME] [-s] [-v PROJECT_VERSION] [--verbose]

Create and manage manifest files.

options:
  -h, --help            show this help message and exit
  -c, --check           Check manifest file
  -i INPUTFILE, --input INPUTFILE
                        Manifest to import
  -n, --new             Create a new manifest file
  -o OUTPUTFILE, --output OUTPUTFILE
                        output manifest file name
  -p PROJECTNAME, --projectname PROJECTNAME
                        Create a new manifest file
  -s, --show            Show manifest file
  -v PROJECT_VERSION, --version PROJECT_VERSION
                        Project version
  --verbose             Make elf more verbose
```

## elfdocker

This tool can be used to manage docker container and images related to elf
```bash
$ elfdocker -h
usage: elfdocker [-h] [-b] [-c] [-s] [--verbose]

Create and run a new container from an image.

options:
  -h, --help   show this help message and exit
  -b, --build  Build docker image
  -c, --clean  Clean docker image
  -s, --show   Show docker image
  --verbose    Make elf more verbose
  ```

  ## elfrun

  This is the main command to run ELF containers

  ```bash
  usage: elfrun [-h] [-c config] [-D path] [-e ...] [-i] [-m MANIFEST] [--no-dot-files] [--no-rm] [-s sharedir] [-u] [-w workdir_path] [--tun] [--http-proxy [proxy_url]]
              [--https-proxy [proxy_url]] [--proxy [proxy_url]] [--privileged] [--verbose]

Create and run a new container from an image.

options:
  -h, --help            show this help message and exit
  -c config, --config config
                        Configuration to use
  -D path, --cd path    Working directory inside the container
  -e ..., --exec ...    Command line executed inside the docker container
  -i, --interactive     Start with interactive mode
  -m MANIFEST, --manifest MANIFEST
                        Manifest to import
  --no-docker           Do not enter in a Docker container
  --no-dot-files, -n    Do not mount user dot files from home
  --no-rm, -R           Start a persistent docker container
  -s sharedir, --sharedir sharedir
                        Shared directory
  -u, --usb             share usb to the container
  -w workdir_path, --workdir workdir_path
                        Bind mount the workdir as a volume
  -t, --tun,            Make the TUN control device available by duplicating
                        /dev/net
  --http-proxy [proxy_url]
                        Set the http proxy, if proxy_url is not set, uses the HTTP_PROXY environment variable
  --https-proxy [proxy_url]
                        Set the http proxy, if proxy_url is not set, uses the HTTPS_PROXY environment variable
  --proxy [proxy_url]   Set the proxy, if proxy_url is not set, uses the HTTP_PROXY and HTTPS_PROXY environment variable
  --privileged          Enable privileged mode
  --verbose             Make elf more verbose

The additional arguments are forwarded to "docker run",
see "docker run --help" for more details.
```

  ### Ubuntu releases
The default release is now Ubuntu 24.04 (Noble Numbat).
It's possible to configure the release in the manifest :
```xml
<?xml version="1.0" encoding="UTF-8"?>
<manifest version="1">
  <default template="elf-ubuntu-2404" />

</manifest>
```
Available releases are the following ones : 
  - "elf-ubuntu-1804" (_Bionic Beaver_)
  - "elf-ubuntu-2204" (_Jammy Jellyfish_)
  - "elf-ubuntu-2404" (_Noble Numbat_)

However some plugins require a specific release, so you won't be avaible to configure yours.

### test command

Some manifests contain test fields so you can test some project features. Simply use :
```bash
elfrun [-options] test
```